# Templates

Each [branch](https://github.com/murksa/templates/branches/all) corresponds to a template. To fetch a template on Windows, use
```
git clone --depth 1 --branch <TEMPLATE> https://github.com/murksa/templates <OUTPUT-FOLDER> && rmdir /s /q <OUTPUT-FOLDER>\.git
```

You might want to put this to a batch file:
```
set branch=%1
set folder=%2
git clone --depth 1 --branch %branch% https://github.com/murksa/templates "%folder%" && rmdir /s /q "%folder%\.git"
```
